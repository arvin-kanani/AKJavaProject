package com.example.javatest.Arrays;

public class Students {
    public static int[] IDs(){
        int[] ID = {
                1,
                2,
                3
        };
        return ID;
    }
    public static  String[] getFullNames(){
        String[] FullName = {
          "Ali Moradi",
          "Farzad MohamadKhani",
          "Maria Melory"
        };
        return  FullName;
    }
    public static  boolean[] isIranian(){
        boolean[] isIranian = {
                true,
                true,
                false
        };
        return isIranian;
    }
    public static double[] score(){
        double[] score = {
                18/75,
                19/5,
                20
        };
        return  score;
    }
}
