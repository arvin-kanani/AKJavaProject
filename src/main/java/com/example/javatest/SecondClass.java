package com.example.javatest;

import com.example.javatest.Arrays.Students;
import com.example.javatest.Utils.AKSystem;

public class SecondClass {
    /*
    AKSystem.Write/WriteL : a method of System.Out.Print in Utils Package

    Original class : We can run just this kind of methods
     */
    public static void  main(String[] n){
        FirstClass.firstMethod();
//        System.out.print("\n");
        FirstClass firstClass = new FirstClass();
        firstClass.lastName("kanani\n");
        SecondClass.Show("Hello\n");

        //Now from Arrays package:
        int[] studentsID = Students.IDs();
        AKSystem.WriteL(studentsID[2] + "");

        String[] fulltudentsName = Students.getFullNames();
        AKSystem.WriteL(fulltudentsName[1]);

        boolean[] isStudentsIranian = Students.isIranian();
        AKSystem.WriteL(isStudentsIranian[0] + "");

        double[] studentsScore = Students.score();
        AKSystem.WriteL(studentsScore[2] + "");

        AKSystem.WriteL(getStudentsNameValue() + "");
    }
/*
message: give programmer or user message

this method write programmer or user message(write message string)
and if message is null then it write "Error"
 */
    protected static void Show(String message){
        if(message != ""){
          AKSystem.Write(message);
        }
        else {
            AKSystem.Write("Error");
        }
    }
    public char charMethod(){
        AKSystem.Write("I'm a Char Method");
        return 's';
    }
    public static  int getStudentsNameValue(){
        return Students.IDs().length;
    }
}
